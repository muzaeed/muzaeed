import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  cartItems: any;
  orderSummary: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://run.mocky.io/v3/ca987707-db46-419e-9178-2bbdde3c3762')
      .subscribe((resposne: any) => {
        if (Response) {
          let resposneFromAPI = resposne;
          this.cartItems = resposneFromAPI.cartDetails.cartItems;
          this.orderSummary = resposneFromAPI.cartDetails.cartSummary
          console.log(this.cartItems);
        }
      })
  }

}
